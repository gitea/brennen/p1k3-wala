(Got this from [http://plasmasturm.org/ Aristotle Pagaltzis], incidentally, but apparently it's been making the rounds for a while.)

<[Stephen]>
   89   cd
   81   svn
   72   ls
   49   ssh
   30   open
   19   mv
   18   mate
   17   ping
   13   ps
   12   rm

Open is from the mac environment. From a terminal window I can just do a quick 'open .' to open the directory in Finder, for example.

I am a subversion junkie.


<[Brent]> Out of curiosity, why does this suggest a poorly-configured bash?

<[Brennen]> 1) History was relatively short - I bumped it up to 10000 entries immediately after running this, and want to look into whether there's a way to make it effectively infinite, or at least start periodically archiving it. 2) I'd prefer that it ignore instances of ls and cd by themselves. 3) I should (and usually do) have some things aliased to single-letter commands (vim, ls, some hosts I commonly ssh to).

<[Brennen]> There's some useful stuff on configuring Bash history [http://www.gnu.org/software/bash/manual/bashref.html#Bash-History-Facilities in the manual], in a [http://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html#index-HISTCONTROL-188 couple of places].

Also: [http://www.talug.org/events/20030709/cmdline_history.html Using Bash's History Effectively], and [http://www.deadman.org/bash.html Advancing in the Bash shell].


<[Brent]> Thanks for the info!

Just to illustrate how paranoid I am, I then went and disabled history in my /etc/profile.  Don't like the idea from a security perspective; all those commands saved somewhere to disk where a hacker can read them in plain text?  :shiver:


<[brennen]> YMMV, but I'd feel fairly crippled at this point without a searchable history. I must hit ctrl-r 20 or 30 times a day. I suspect that if someone gets to the point where they can read my .bash_history, I'm probably already screwed.

The place where I imagine this kind of paranoia is justified is entering password info on the commandline. In the general case I don't think this should happen, but one good workaround would be to set HISTCONTROL=ignorespace, which means that any command prefixed with a space will be left out of the history. This would also be useful for commands you wouldn't want to leave lurking in your history because they're dangerous (rm -rf anyone?) or embarrassing (mplayer ./SCARY_SCARY_PORN.MPEG) or what-have-you.


<[Stephen]> ctrl-r is essential for my work. I agree that keeping passwords out of the history is ideal, but on the excessively paranoid end. As you say, if they can get to .bash_history then it's already game over.


<[Alan]> As a rule, I never specify a password on the command line. Any tool that requires one but doesn't have an option to read it from an fd is seriously sketch. Especially in the presence of obsessive typers of "w." ;)
