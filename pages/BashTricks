WareLogging. SourceCode. BASH is the BourneAgainShell. In terms of scripting, I tend to think it's considerably more sane to write Perl these days, but I promise you Bash is not going away any time soon.

= keys & builtins =

* Press ctrl-r for an interactive search of your command history. This is usually faster than arrow keys, and probably safer than guessing at a bang completion.
* esc, . (escape, followed by a period) will pull up the last word of the last command
* "cd -" - bounce back to the last directory.
* Redirect STDERR: "command 2> file"

= references & tutorials =

* [http://www.ibm.com/developerworks/library/l-bash.html Bash by example, Part 1]
* [http://www.cs.uu.nl/wais/html/na-dir/unix-faq/shell/csh-whynot.html csh programming considered harmful]
* [http://www.shelldorado.com/ Heiner's SHELLdorado]
* [http://talug.org/events/20030709/cmdline_history.html Using Bash's History Effectively] - A few cool tricks here I wasn't aware of.
** export HISTIGNORE="&:ls:[bf]g:exit"
** 'shopt -s cmdhist' - record multiline commands as a single line in history
** history | grep -i "<search string>"
** !<history number>

= aliases =

To investigate: BashFunctions.

From several versions of ~/.bashrc:

    # stack-based directory navigation:
    # c to change, b for back, u for up.
    alias b='popd'
    alias u='pushd ..'
    alias c='pushd'
    
    # frequent commands:
    alias m='mutt'
    # p used to be pine, but I quit using pine.
    alias p='ssh p1k3.com'
    alias v='vim'

    # these might break using GNU date(1), I should really check.
    alias t='cd /www/p1k3/archives/`date +%Y`/`date +%m`'
    alias tv='vim /www/p1k3/archives/`date +%Y`/`date +%m`/`date +%d`'
  
    # colorized & shortened ls
    alias ls='ls --color=auto '
    alias ll='ls -lh'
    alias la='ls -A'
    alias l='ls -CF'
    
    # for those MS-DOS kinda days
    alias dir='ls --color=auto --format=vertical'
    alias vdir='ls --color=auto --format=long'

    # command prompt
    # user@host:working_directory$
    PS1='\u@\h:\w\$ '

= .inputrc =

This stuff is crucial: Kill the obnoxious beep, and cycle through possible completions rather than requiring 2 presses of the tab key to display a list of choices.

 set bell-style none
 set completion-ignore-case on
 set completion-query-items 150
 set mark-directories on
 set visible-stats on
 TAB: menu-complete

= Constantly refreshing display of command output =

I often want to monitor a file or service in real-time.  This lets me do it:

 while [ 1 ]; do { clear; tail -20 /var/log/messages; sleep 1; } done

This will display the last 20 lines of /var/log/messages on a blank screen, refreshing every second.  Replace "tail -20 /var/log/messages" with the command that you want to refresh.

<[[Brennen]]> As another approach, [http://www.gnu.org/software/coreutils/manual/html_chapter/coreutils_5.html#SEC19 tail(1)] itself offers the -f / --follow option, so for example,

 tail -f /var/log/httpd/access.log

At least some versions of tail support following multiple files at once. These may be GNU specific options; I'll look it up at some point.

There's also watch(1), which is essentially the above loop for whatever command you feed it.


<[[Brent]]> Very cool!  I created the above trick because I've been on systems on which watch wasn't available, and I wanted to watch a process (say, "ps -ef | grep sql") instead of a file.

It's also nice to have it clear the screen each time.

= In-place Files =

In attempting to automated FTP uploads, [[Brent]] has had trouble passing data to the ftp command.  An easy solution:

 ftp -n -i (HOST) <<- endcommands
 USER (USERNAME) (PASSWORD)
 cd wwwroot
 mput *
 bye
 endcommands

The -n parameter suppresses auto-login, and -i turns off prompting.
