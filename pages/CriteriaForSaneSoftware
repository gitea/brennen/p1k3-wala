TechnicalIdeaLogging.

I am not going to live in your software. I am especially not going to ''commit'' to living in your software in order to see if I ''might'' be interested in using it. 

Here is how you can tell if I will be interested in using your software to cope with photos, songs, or text: Does it create its own specially formatted database or filesystem, copy my data into it, and perform all of its operations on this copied data, leaving me to cope with a lossy export mechanism or use third-party tools when I migrate to something else? Then I'm not interested. I have a filesystem, and I have a decade's worth of data on that filesystem, and I don't trust you to be smarter about either of these things than I am. I also don't trust your software to live longer than my data. Just because Apple gets away with it doesn't make it a good idea.

Here are some possible quasi-exceptions: Source control and filesystems. Are you writing one of these? No? Then knock it off. If you ''are'' writing one of these, what can you do to minimize how much my stuff is locked into your system? If this isn't a driving concern, then I probably won't be using it, whatever it is.

I am not going to have an exclusive relationship with your software. This is why text files still win, and it is why you should never assume that I will invest effort in anything proprietary to your code, even metadata. Life is too short to deal with walled-garden bullshit when there are other options. Just because Facebook gets away with it doesn't make it a good idea.

[http://blog.plover.com/misc/census-data.html Data files should contain data.] (But if you lose things in my data that don't look like data to you, and neglect to tell me about it, I'm going to be pissed.)

If any element of your design flows from a desire to prevent me from doing useful things I will obviously want to do with my hardware & software, then your design is worse than useless. It is a cringing, penny-ante piece of evil.

Are you trying to put more advertising in my field of vision? Knock it off.

Defaults matter.

There had better be a man page.

Don't make me read the man page.