SeeAlso: GraduatePrograms, AlbanyFreeSchool, DemocraticEducation, AlternativeEducation, PhilosophyOfEducation, HistoryOfEducation, DemocraticSchool

Pursue further understanding of:

# the factors which influence current models of education, including but not limited to:  social, philosophical, economic, religious, cultural, political (focus will be on the U.S. with acknowledgement of international influences)
# a history of education, both traditional and alternative, public and private
# ways that alternative education has succeeded and failed, qualitatively, in the last century
# the differences between alternative and specifically democratic philosophies of education
# the impact of traditional vs. alternative education on groups of children between ages of 3 and 11

* how to do this?

Towards what end?

# A clearer grasp of my own educational ideology
# A written work of substantial worth on the place of alternative education within today's society
# A plan of action for my own "contribution", whether as a teacher or as a school founder or both, throughout my lifetime
# A holistic understanding of the issues confronting education, both of how educational systems fail and how they succeed and what I mean by "succeed"
# An ability to relate the works of educational theorists, educational philosophers and school policy makers to each other
# A way to resolve the question of "demonstrable evidence" for "success" in alternative educational systems, without buying into identical patterns of language used to describe "success" in terms of traditional or conventional thought, within corporate models or towards a "productive" end
# An attempt at defining what is important for children, holistically, as humans who need to be prepared to interact well, function optimally, create endlessly, and know their own worth and place in the world.  

* globally, locally

<[[Jeremy]]> I wonder if it would help to work on your goals via an example.  If you could work in the ideal job or form the ideal school, what would it be?

Now, what challenges does that ideal present: currently and historically?
<[[Elizabeth]]> Hey Jeremy, 

I've been thinking about your questions and before posting a response, I want to add the body of a letter I just wrote.  This is in response to the question of "what are your career goals?" and feel that it will help to have this out before anything else.  Thanks for helping me to develop this into a dialogue.  You're really helping me to clarify my purpose.

Letter below:

I should start by saying that my career goals are very much in the midst of being shaped right now.  I have taught in two separate alternative schools since graduating college, both of them democratic free schools.  The first was the AlbanyFreeSchool in AlbanyNewYork and the second was the TamarikiSchool in ChristchurchNewZealand.  Both schools are based loosely on the Summerhill model of education that began in England ( A.S. Neill) in the 1920's and on similarly progressive education models and thought.  

My time at the Albany Free School was deeply enriched by my discussions with the established teachers there, as well as the community surrounding the school.  It was a year of serious inquiry into the current state of traditional education, as well as learning hands-on how and why freeschooling approaches "education" from a radically different angle.  The experience led me to further my understanding of this radical approach to education by going global, and I followed my time in Albany with a 4 month traveling adventure in India, the UnitedArabEmirates and NewZealand.  

BhubaneswarIndia was the location of the annual InternationalDemocraticEducationConference.  This gathering of educators from around the world fostered incredible conversations on education, both in theory and practice.  I followed this conference with a visit to a friend who is teaching philosophy at the AmericanUniversityOfSharjah in the UAE.  There I had the privilege of doing my first presentation on democratic freeschooling during a week-long education forum at the university.  Finally, I traveled to New Zealand to spend time volunteer teaching at the Tamariki School, where I again found myself surrounded by the radically different environment of a free school.  As well as an amazing group of Kiwis!

These are the experiences that have shaped my outlook on education and really, my outlook on life.  I have formed serious questions about the state of education in the United States and have compared what I know about our system with international systems.  I am interested in spending a solid amount of time doing concentrated research, in order to further my understanding of alternative methods of educating that include, but are not limited to, freeschooling.

All this is towards an end of what, I'm not entirely sure.  I have wanted to start a school for a while now, but feel that I will need more experience teaching after grad school.  I don't want to limit myself only to teaching, however; I feel that part of my contribution is supposed to come from a deep inquiry into the hows and whys of educational theory and practice.  Perhaps I will write, or work in a think tank.  I think more than anything, I am looking at grad school as a way to open up my options, to challenge me to look at things in a less nuclear fashion and to refine my understanding of what optimally would happen during a child's education.