<[[Stephen]]> Damn low end of the labor market.

My situation is only slightly better. Not much middle management but I am caught in a grey area between library school graduate students doing menial work and graduated librarians doing more professional work.

You know how staff is marginalized in a university environment of students and faculty? 

My department is that in microcosm where the only hope of advacement is a master's degree.

<[[Brent]]> Interesting.  And here I am, working in a company where junior engineers can work their way up the hierarchy pretty darn quickly.  Heck, the current CEO started out as a low-level engineer at a previous incarnation of the company.

<[[Brennen]]> I suppose if I were to make any point ''contra'' this last thought, it would be that the mobility you describe applies to people who have attained the status of "engineer". In an environment like the one I quit in meaningless protest this morning, none of the people in the building doing the truly shit jobs have the faintest hope of access to mobility. The organizations that hire & fire them are designed to isolate them from it. This was mostly my point - not about the kind of jobs you get with a respectable degree in an appropriate field and/or a long, impressive work history, but about the janitorial, very low end clerical, mailroom, etc. kinds of jobs.

It should be (and supposedly once was, maybe still is at some companies) possible to view these jobs as actually being entry level positions, in the sense of being the low rung of some climbable ladder. My experience tends to suggest otherwise. Granted that my experience is filtered through a temp agency - it also seems to me that a large and increasing proportion of these sorts of jobs are filled through temp agencies.