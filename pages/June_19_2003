<[[Saalon]]> Oh no.  Are you coming down in favor of FreeVerse being more artistically viable?  Admittedly I know too little about poetry to say wholeheartedly that freeverse is crap but...ok...99% of all poetry not following some sort of rule structure, either meter or rhyme or something, has been supercrap.  The most intelligent thing I can say is that my two favorite poets, LordAlfredTennyson and RobertFrost, have both adhered very closely to some sort of structure, and usually it rhymed (though not agressively so).

And don't take this aggression personally.  It's early and I'm feeling fighty.  Mostly I like you enough to argue with you.

<[[Brennen]]> Because I am a touch ignorant about these things, I took a quick look at Wikipedia:Free_verse and Wikipedia:Prose_poetry. To quote both brief entries:

:Free verse (or ''vers libre'') is a style of poetry, usually unrhymed, which retains the rhythmic cadences of traditional poetic meter, without strict adherence to traditional poetic forms. As put by Ezra Pound, ideally "As regarding rhythm: to compose in the sequence of the musical phrase, not in sequence of a metronome."

:Prose poetry is prose that breaks some of the normal rules of prose discourse for heightened imagery or emotional effect. ... Much contemporary poetry is written in free verse, and the difference between much free verse and prose poetry may be more in the typography than in the content.

I would not suggest that there's anything like a last word on this stuff, but I guess those might be useful definitions. On the other hand, I have heard free verse used to mean something that is clearly prose (or prose poetry) using a simplified structure with a lot of line breaks. I don't have a problem with this usage, because people seem to know what it means, and also because writing that way leads people to pay attention to rhythm whether they know it or not, which produces something I think looks a helluva lot like verse. "May be more in the typography than in the content" is probably the most significant thing there - and it is important to remember that everything which uses its own typographical conventions is not trying to be poetry, even if it succeeds.

No, I'm not going to take a stance on the artistic viability of freeverse, at least not to say it's any greater than that of sonnets, quatrains, haikus, and whatnot. I will suggest that its less specific nature makes it more ''flexible'' than any of those forms, but there is a tradeoff.

 keeping in mind that all poetry is not verse,
 the point of verse is that poetry arises from the tension
 between imposed structure
 and creative expression

So, ok, "99% of all poetry not following some sort of rule structure, either meter or rhyme or something, has been supercrap". You are far from wrong, but ''some sort of structure'' is what matters. Freeverse ostensibly has fewer rules, but the truth is that some "rules" are just implicit. People ''feel'' rhythm, language, and typographical conventions, even if they can't state them as algorithms. Some rules can simply be made up on the spot. Yes, it is hard to create something great because structure is hard to find or because people think they don't have to find it, but it ''is'' there, and so freeverse works - or it can.

Poets like [[Kenneth Rexroth]], [[Charles Bukowski]], [[E.E. Cummings]], and [[Sydney Lea]] appeal to me for different reasons, but they tend to have in common that what they say hits me where I live without beating me over the head with its form. Cummings might be an exception, but Cummings' form usually says something and it is usually interesting. I do not find a sonnet's structure interesting in and of itself.

Aside from Bukowski, freeverse is not the first thing that comes to mind when I think of their work. They all write structured poems; sometimes in traditional formats, though more often not.

Anyway, most of my favorite poets rhyme a lot too, if you count RiversCuomo, BobDylan, and WoodyGuthrie. The great truth about modern poetry is that our poets are songwriters, and it has been true for at least half a century.

:''And let's not forget GregBrown.''

----

Also, http://www.poets.ca/linktext/links/articles/sidemaking.htm

:Very little free verse is pattern-free and I can't think of a single successful poem that is. People who don't read much poetry and who still write it tend to write either pretty rhyming poetry (the surface of which is contorted by for need to rhyme and regular rhythmic patterns -- which makes me want to distinguish between "verse" and "poetry") or confessional/confrontational poems (where the language is unshaped and the deep emotions are meant to carry the poem but such poems are hard for the reader to enter into -- which makes me want to distinguish between poetry that helps people experience the event and emotion and that which talks at them). I exhort these writers to read poetry. Lots of it. And see what they can learn about what distinguishes poetry from other words on a page or spoken aloud.