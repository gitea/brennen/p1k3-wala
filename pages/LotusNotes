WareLogging.  Groupware, which is to say a very slow e-mail system with a raging superfluity of inefficient capability and a lot of metaphors borrowed from paper document systems.

<[[Brennen]]> Or am I being unkind?
<[[BrentNewhall]]> No, though Lotus Notes can be amazingly useful if set up right.  I am blessed (I suppose) that I work at a place where Notes is set up right.

Lotus Notes can be seen as a large document-based conversation, similar to the early online services such as CompuServe and Delphi.  With consistent application of the proper metaphor, Lotus Notes can be a powerful system.

Or -- as seems to be much more common -- it can suck away your productivity with the inevitability of a black hole.

<[[Brennen]]> Yeah, that's the alternative I'm familiar with. Fortunately I didn't have to ''use'' Notes for anything except occasional e-mails from my boss.
