IdeaLogging, IntellectualProperty, PublicDomain.

<[[Brent]]> The only example I can think of is Wikipedia, which has a little boilerplate on the "post" page, which advises writers of the copyright under which their text will be placed when posted.

This is fine, except that Wala has text boxes at the bottom of every page.  You could add a little link below the text box, something like "Your content will become public domain", which would link to a page explaining your stance on copyright and the fact that, well, stuff posted on here is to be considered public domain.

Side question:  Why do you believe that copyright is not automatic upon creation?  I always felt that this makes sense.  The creator can immediately give up that copyright if desired.

_(<[[Brennen]]> See response below...)_

Perhaps we need a finer-grained set of legal terminology.  I'd like a legal appreciation of "creator," which may or may not have different automatic rights than "copyright holder."

For what it's worth, no, I have no problem with my writing on this wiki being made public domain.  I assume that pretty much everything I post on the web is public domain.  Which is why I don't post everything I write on the web.

<[[Stephen]]> Copyright shouldn't be automatic upon creation because copyright is simply too much, too long. This system worked just fine when copyright terms were limited to 14 years extensible once for 14 years. I argue that those orginal limits would be completely adequate now. 28 years is plenty of time for a videogame, movie, book, photograph, etc. to make enough money to repay the creator (should the creator seek monetary compensation). A movie released today would be in the public domain in 2020 (or 2034). That's enough time to release the movie to theaters, release a dvd, release a special edition dvd, release a 10th anniversary dvd, (and a 20th and 25th). Can anyone seriously argue that this wouldn't be an adequate return? For computer software those terms should be halved.

As it is now, copyright is 70 years after the death of the author or 75-90 years for works by corporations. Unfortunately for us, many many many works will be made by corporations that won't release the rights to the public domain and will cease to sell those works (or those works will cease to be available).

Two examples: cartoons and video games. There were hundreds of cartoons made during the golden age of cartooning of the 20s-40s and many of these weren't made by Warner Bros. or Disney; but since Disney has consistently and successfully lobbied for copyright extensions the two times that Mickey Mouse was due to enter the public domain these unmaintained works are still illegal to sell or trade online. Video games of the 1980s and early 90s that are now no longer available to the public (called abandonware) are in a similar legal predicament. People want to enjoy these games, but can't buy them new and can't buy them used in many cases. Enterprising fans who want to remake the games would have to break the law to do so.

If copyright required some action on the part of the creator this would be one way to fix this problem. But I propose that copyrights should at least require maintenance. As a copyright is due to expire, the holding creator or company could pay a nominal fee (or just write a letter or perform some other action to show that the copyright is actively used) to keep the copyright. If no response is made then that work should pass to the public domain.

I think it's worth noting that as it stands now, no works will enter the public domain which are not explicitly placed there. This is a cultural tragedy of the greatest proportions, and it should be addressed.

Oh, and I'm perfectly fine with anything I'm posting going to the public domain. I always assume that anything I put on the Internet is in the public domain. In an international network, why would someone in China care what U.S. Code Title 17 says they can do with my text?

<[[Brennen]]> Re: Brent's side question from above, as I understand the law, copyright ''is'' automatic on creation. Taking the question to be "why do I think this is a bad idea"; I used to feel pretty much as Brent does. I think Stephen outlines the way I think these days pretty well, though I'd also recommend that anyone interested in this debate grab a copy of Lessig's ''FreeCulture''. He does a good job laying out the undesirable consequences of extending copyright automatically to every creative act. He also makes the more subtle point that the vast reach of digital communication has extended copyright law into pretty much every creative realm as an unintended consequence of an architecture based on copying.

Also:

> Perhaps we need a finer-grained set of legal terminology. I'd like a legal appreciation of "creator," which may or may not have different automatic rights than "copyright holder."

I think there's definitely something to this. Though I am increasingly disinterested in social engineering via law, I can imagine legal frameworks that creatively address the problems copyright law was originally intended to solve. But I'm quite convinced the current framework is no longer being directed toward those problems. It has become a mechanism for the concentration of economic & creative power, plain and simple.

<[[Brent]]> Everything eventually becomes a mechanism for the concentration of economic power.  That's just how humans are.

Which is not to be depressing; just pointing out that this is neither new nor unique to this particular space.  I'd rather come up with a solution than dwell on the theoretical social ramifications of the current model.

Here's what I'd like to see:  A work's Creator has automatic rights to control the distribution of the work (roughly similar to original copyright law rights).  After, say, 14 years, the rights turn into copyright, which the Creator or someone else (if the Creator has died or gives over ownership or clearly shows no interest in continuing control) can then have for 14 more years.  After that, it's public domain.

<[[Stephen]]> That's actually what the founding fathers wrote into the Constitution. Except I don't understand your distinction between the Creator's "automatic rights" and copyright. Copyright is literally the right to control the creation of copies and distribution of the work. Whenever something is published by a company the creator has directly or indirectly sold the copyright to that company.

And again, I'd like these numbers halved for software. I'd say that (for example) Wolfenstein 3d and the original Mortal Kombat (released 14 years ago in 1992) have made just about all the money that they're going to make for their creators.

<[[Brennen]]> I think the scheme Brent is proposing is one where a "creator" status is non-transferable, at least for the first span of time. It's an interesting idea, and I've argued over similar variations in the past. I guess I suspect that it's an interesting thought experiment without a great deal of applicability to the current system - unless it were implemented in some way external to the legislative system, like a voluntary licensing scheme.

<[[Brennen]]> Will have to elaborate on this when I get off work.

<[[BrentNewhall]]> Brennen's basically correct.  I'd like there to be a generally-accepted (and legal!) distinction between the Creator -- who never changes -- and the copyright owner.  The Creator has the rights for an initial period of time, and has the option of extending those rights or selling/giving them to another party for another period of time.

I'm using 14 years as a convenient number, not because I believe in it strongly.  I hold myself to a 5-year copyright limit for my works.  Any of my writing that's more than five years old goes onto my website, and into the public domain.  http://brent.other-space.com/writing/

(...gah!  And that led to a two-hour detour as I updated a bunch of content on my writing website.)

<[[Brennen]]> Like I said above, I think it's an interesting proposal. There could be real benefits in assigning certain non-transferable rights to creators. On reflection, though, I can see a couple of practical issues even once you get past the fact that it would destroy or greatly weaken the power of every corporate vested interest in the current system. (I'd like to do the same, but I'm not about to believe that the Powers What Be will let such a thing come to pass via lesgislation.)

Anyway, if creatorship were a sort of immutable quality in the eyes of the law, I think you'd run into all sorts of weird border cases and intractable problems. Who is the "Creator" of Wikipedia? The Linux kernel? And surely there would be a practical distinction between the way copyright is assigned in exchange for financial compensation now and under a creator-based rights system, but what would it be?

Also, what are the chances that a mixed system that allows such assignment/sale of rights after an arbitrary period wouldn't become even ''more'' burdensome in terms of tracking permissions? I've spent a lot of time now dealing with the real overhead of one complex & inconsistent system; I can well imagine the ways it could become worse. Re-using material is presently slow, laborious, expensive, and risky - this in a world where the _technological_ ease of re-use is greater than at any time in history. Imagine if the system were one in which two entirely different models of compensation/re-use/rights-assignment/contracting had to be undertaken for the most profitable phases of a given work's life cycle.

(Note that above I'm not just talking about re-use of public domain / fair use stuff / other people's content in general. I'm talking about what happens when, say, an academic publisher wants to reprint an edited volume of essays on a given topic, with illustrations. Can you imagine the complexity of a reprinted volume where half of the contributions have switched modes from creator-oriented permissions to reassignable copyrights?)

<[[Brennen]]> A further [http://p1k3.com/2006/3/7 post on the topic].

<[[Brent]]> Copyright is already re-assignable, as I understand it.  Under the present legal system, I can write a story, then transfer its copyright to anybody.  And they can transfer it to others.

This already happens quite a bit.  Heck, the broadcast rights for a given work are already complicated, as the recent rights to Earthsea have shown (Studio Ghibli couldn't make their own anime movie out of Earthsea until the Sci-Fi Channel had made their live-action miniseries, and that's separate from the regular book printing rights).

<[[Brennen]]> I think we might be talking past one another here. I am aware that copyright is re-assignable / salable / whatever, though I'm far from up on all the ins and outs. I was talking about your hypothetical split model with 14 years of "creator's rights" and an extended period of sell-it-to-someone-else renewal.

<[[Stephen]]> Re: Creators Rights/Copyrights

What I still don't understand about the Creator's Rights is: what is the Creator doing for that first span of time where (s)he holds the rights? If the Creator is allowed to let a distribution company make copies of the works for sale, how is this not a copyright in the first place? If the Creator isn't allowed to have a distribution company make and sell copies, then does the Creator have to do it themselves?
