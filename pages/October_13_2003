:''Which leaves the systems that really are something like that notebook on a table. How would we go about implementing a kind of handwriting on a wiki?''

<[[Stephen]]> Handwriting is reflective of training in the real world. There are an infinite number of ways that a person can write the letter "e", but even though there are just as many different ways to hit the letter "e" on a keyboard, it's all the same to the computer. Unless the keyboard is recording a whole bunch of tactile information that translates into an electronic "handwriting" then the system HAS to be based on user choices.

"Primary Inversion" by Catherine Asaro (who happens to homeschool her kids and order CAT tests from my parents, but I digress) is a book of decent scifi that does contain a few interesting ideas. A personalized Internet interface is one of those. Every person configures their computer to display information a certain way (font, color, theme, etc). We could use that system and instead have information put on the Internet by individuals to be set to display according to rules that the writer creates.

But this makes theme changes difficult (changing a computer theme is trivial, changing your handwriting is much more difficult) to track -- would *everything* that a user had written change its display with every theme change? Or could a person change thru different styles at will and appear to be multiple people?

As I ramble, a solution appears. Just use GPG and have done.

----

<[[Brennen]]> The rambling is good.

As far as a personalized ''web'' interface goes, if you want it enough to deal with the hassle it seems to be available now - the important free software browsers (Mozilla and Konqueror, plus their derivatives) seem to support customized user stylesheets applied to web pages. I am not actually sure how good an idea that is much of the time, but as Opera amply demonstrated a few years back the ability to apply an arbitrary rendering to otherwise unreadable pages is a great feature. I come across fewer unreadable pages these days, but if I were, frex, extremely color blind, that's still one of the first things I would demand in a browser interface.

At any rate, it's not too big of a stretch to imagine people configuring custom stylesheets for their own text (font, back- and foreground color, weight, width, indent, etc.) - I do that much with blockquote elements for a few weblogs I quote, and I think it works acceptably. The problems I see with this are,

* As you mentioned, changes in theme raise difficulties. I think these could mostly be solved by making a decision and enforcing it consistently throughout the system. Either make changes to themes system-wide and tied to an identity, or (my preference) retain the appearance of each edit.
* More seriously, custom themes require storing a lot of information and transferring it between machines. Obviously enough a theme could be stored server-side (in fact this makes way more sense as long as we're talking about a single wiki-like system), and tied to a password protected account with a full set of preferences. For reasons I'll get to in a minute, I'm not satisfied that this would constitute "handwriting" in the sense I'm looking for, and it violates one of my criteria for a useful wiki, which is minimizing the barriers to entry as much as humanly possible without removing them completely. Which brings us to GPG...

GPG and its kin are nifty, and in a technical sense they go a long way towards solving all kinds of problems. Unfortunately, they are also ridiculously hard to use by the standards of anyone less skilled and patient than (roughly) me. By geek standards I am neither exceptionally skilled nor particularly patient; by normal user standards I maintain a level of proficiency which is literally beyond comprehension, and even post-techie-burnout, I am probably willing to spend more time with this stuff than all but a few tenths of one percent of the computer using population.

At any rate, when I was thinking of handwriting, I wasn't really thinking of signatures in the sense of absolute and absolutely trustworthy indicators of identity. I probably didn't articulate this very well, but I had more in mind things that would feed reasonably well into our existing heuristics for authorship without requiring much conscious intervention on the part of a writer.

(The level of preference-setting I have in mind is demonstrated pretty well by Brent's [http://www.stun-software.com/wala/ Wala], where users set their names with a single, non passworded, input box - a direct analog to the IRC interface that it also mimics when appending text to a page. You'll notice I'm seldom logged in here, basically because UseModWiki's interface is really terrible in spots and logins are one of them.)

Given the constraints of using unique information about a writer (obtainable by a web server without specifically asking for it) to visually indicate authorship on a system as generally anarchic as a wiki already is, I think I have either written myself into a corner or decided to overthink an already-solved problem. Still, it makes for kind of an interesting thought exercise.

----

<[[Brent]]> How big of a problem do you want to solve?

As a local solution, this is relatively straightforward.  For example, look at something like a Wala.  Imagine if each contribution were surrounded by a <div> tag with the ID of the contributor?  So, we might have something like this:

<div id="Brent"><[[Brent]]> A beginning is the time for taking the most delicate care that the balances are correct.</div>

The site's stylesheet can specify how it wants Brent to look, and users can set their own stylesheet to specify how they want Brent to look.  Problem solved, on that level.

What about a larger solution, though?  How do we do this in general, across all webpages, especially when some people are named funky things like "div"?

Well, one simple solution would be an extension of the above; prepend each <div> with, say, author:

<div id="author-Brent"><[[Brent]]> A beginning is the time for taking the most delicate care that the balances are correct.</div>

This could be part of any webpage, and would uniquely specify that person's text.  The user could specify in a stylesheet how to render author-Brent.

This would require a huge improvement in the current interface to user-written stylesheets.  I'm envisioning a custom widget in the browser for defining the look'n'feel of different users.  I see a list of usernames on one side of the widget, and a set of font/color/etc. controls on the other side.  Select a name, and change the font/color/etc. to change how that person is displayed.

<[[Stephen]]> Remember footnote number 2. Applying a div tag implies user choice.

<[[Brent]]> How so?  Seriously, I don't get it.

----

<[[Brennen]]> For what it is, I kind of like Brent's widget/div idea - the widget hadn't occurred to me, but the div thing had. Actually, one thing I thought of doing was adding a tag to a wiki that would delineate authorship of a block of text, which might do other things as well but in practical terms probably means enclosing the HTML for that block in a <div> styled differently for each author.

I thought about adding an authorship tag because I wasn't certain how else to delineate things - if you start to think about the basic technical issues in assigning authorship to portions of a wiki page, they could get kind of hairy. The problems with this should be pretty obvious for anything but a threaded or IRC style discussion, which isn't really where I see a need for handwriting. (Maybe I had better say a *use* for handwriting.)  Collaboratively authored pages that have gone through a lot of refactoring (I have problems with the idea of refactoring a discussion, but that's for another day) are pretty hard to break up in neat little chunks between authors, after all.

I'm thinking out loud here and I should be leaving for class. Anyway, the reason the system Brent outlined doesn't really work for what I'm thinking of as handwriting is that it requires too much conscious intervention. Mechanically, I think the style thing could be done with something like those user-specific <divs>. In fact, I'm not sure how else you'd do it in HTML. I can even think of fairly simple ways it could propagate across servers, but there's got to be another layer to the system.

Right. Really gotta run. I'll probably wind up making notes on this in lecture. More later.

<[[Brent]]> Whose conscious intervention is required in the div system I proposed, and what conscious intervention is required?

As I see it, the writer doesn't have to do ''anything'' other than write like normal.

:<[[Brennen]]> Given that you haven't described a *complete* system where that's true, I will take that at face value and say ok, that's part of what I'm aiming for. Other elements include no requirement of intervention by users (so it's ok if settings are tweakable, but not if users feel like they have to think about it).

:What makes this a little more complicated is the additional requirement that handwriting should reflect some kind of information about the writer, and...  Ok, I have to run again, but I'll get back to this page later this afternoon and complete that thought.

(I understand the issues with refactoring ThreadMode into DocumentMode; it's not ideal for many domains.  But I think it is incredibly useful in certain domains.  It's inappropriate on a book review site like [http://bookshelved.org/ Bookshelved], but incredibly useful for documenting patterns like on WardsWiki.

:<[[Brennen]]> Agreed, it's extremely useful for documentation, and almost certainly in other domains.)

----

<[[Brennen]]> All right, here's a plan for adapting handwriting to a collaborative writing system, minus a few details (like the code that would make it work, or a design for same).

Text differs visually in the following ways: Font, font size, some aspects of spacing, and color. The look and feel of text can thus be changed, at least, along four axes of information about the user. Taking colors into account, more are probably possible. (I'm leaving font weights and styles out of this, 'cause I think it would be better to let those convey other information.)

A CGI script can probably extract a user's: Client, operating system (iffy but not too difficult), and IP address/domain. 

So here's what happens:

* Users set a nickname/identity the simplest possible way, like on the Wala. Requiring a password to login (and thus restricting ownership of a nick) will be an option for the site administrator.
* Every user, plus a meta-user (Anonymous or Unidentified or Nemo), is assigned one or two elements of a text style based on their username. Probably a color calculated by extracting a number from their nickname and using it to pick from a range of acceptable colors (they have to be readable on a uniform background). This will be applied to all of the text written by that username/account. Yeah, I know, letting people choose would be simpler, but I don't want people to have to think about it, and you get other problems that way.
* Every edit to a given page is assigned the remaining elements of a text style (font, size, spacing) based on the software that was used to make that edit. These are combined with the unique styles for the specific writers of each edit.

So, for example, looking at a page written by three people, a reader might see dark grey sans-serif text for everything Brennen wrote using MSIE on a Win2K machine, dark blue serif text for everything Brent wrote using Opera on BeOS, and a dark green proportional font with a little less space between the lines for everything Stephen wrote using w3m on a Linux machine. The most recent edit might be a point size larger, or background colors might adjust to age, IP address, etc. A couple of refinements that leap immediately to mind are that every div and span should have a title="..." identifier with the name of the writer and the date of the last edit, and that somewhere inobtrusive on a page would be a color-coded list of the contributing usernames.

This would be cumbersome, relatively processor intensive (though stuff could obviously be cached), and produce awful looking HTML. It would involve storing all kinds of information, tied to edits, that normally lives only in server logs. There could be privacy issues. I suspect the readability might be appalling.

But does it sort of make sense?

(Kindly note I'm not asking anyone to actually ''implement'' this monstrosity.)

<[[Brent]]> Yes.  Makes a lot of sense.  Would be a fascinating experiment.

<[[Brennen]]> One I will probably make someday, but there are more pressing matters.
