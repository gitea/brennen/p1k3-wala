<[[Alan]]> [http://giftfile.org/documents/free_software_hackers_intro giftfile]

<[[Brennen]]> Do you have any sense whether anything will ever happen with this?

<[[Alan]]> I work with one of the authors. Probably not, but the prototype they built takes an extremely clever (imho) approach to "the PayPal problem." I think the basic idea needs to be recast and, well, marketed a little more agressively to the opensource community.

As I understand it, it works like this. Let's say you (Brennen) want to buy one of the first New Order EPs from me (Alan) for $20. There's a third nonprofit party with the giftfile infrastructure, let's call them the Arbiter. We both have accounts with the Arbiter.

Brennen -> Arbiter: put $21 in account with Arbiter
Brennen -> Arbiter: I want to buy this New Order EP from Alan for $20
Arbiter -> Alan: hey Brennen wants to buy this EP from you for $20
Alan -> Brennen: puts EP in the mail
Brennen -> Arbiter: hey, I got the EP, give Alan his $20
Arbiter -> Alan: here's a check for $20 (I'm keeping $1 as a fee)

All parties are PGP involved are authenticated via PGP, and all data is PGP signed. It's actually even more clever than this: there's less communication between the Arbiter and the parties involved and more peer-to-peer communication, with Arbiter as signing authority. The Arbiter ends up just accepting checks and cutting checks at the request of the parties involved.


<[[Brennen]]> I like it. Reading the pitch you linked, it struck me as a more decentralized relative of a scheme I was thinking about a few years ago, where you'd throw x dollars in a pot for a given period (let's say 50 bucks a month, whatever), and then build a list of folks to allocate those dollars to for each period. So you just put authors/hackers/artists/whoever on your list, and as long as they're there, they get a little love every month.  By default I was thinking divide the pot evenly, but allow users to weight the allocation if they're so inclined.
