TrackingWriters. CambridgePlatonists. 

* http://en.wikipedia.org/wiki/Ralph_Cudworth
* his [http://www.iep.utm.edu/c/cudwor.htm Internet Encyc. of Philosophy page]
* [http://plato.stanford.edu/entries/cambridge-platonists/ Stanford Encyc. of Philosophy on the Cambridge Platonists]

BookLogging:

* [http://books.google.com/books?id=eorVtobNXTMC&pg=PR1&ots=SnN9ZHZ5KJ&dq The True Intellectual System of the Universe: wherein all the reason and philosophy of atheism is confuted, and its impossibility demonstrated, with a treatise concerning eternal and immutable morality.] Neal Stephenson's got nothing on this guy for titles.

From Wikipedia (well, from the 1911 EncyclopaediaBrittannica):

> In 1678 he published ''The True Intellectual System of the Universe: the first part, wherein all the reason and philosophy of atheism is confuted and its impossibility demonstrated'' (imprimatur dated 1671). No more was published, perhaps because of the theological clamour raised against this first part. Cudworth was installed prebendary of Gloucester in 1678. He died on the 26th of June 1688, and was buried in the chapel of Christ's. His only surviving child, Damaris, a devout and talented woman, became the second wife of Sir Francis Masha. The Lady Masham was distinguished as the friend of John Locke and exchange letters with Gottfried Leibniz. Much of Cudworth's work still remains in manuscript; ''A Treatise concerning eternal and immutable Morality'' was published in 1731; and ''A Treatise of Freewill'', edited by John Allen, in 1838; both are connected with the design of his magnum opus, the Intellectual System.

> ...

> No modern reader can endure to toil through the ''Intellectual System''; its only interest is the light it throws upon the state of religious thought after the Restoration, when, as Birch puts it, "irreligion began to lift up its head." It is immensely diffuse and pretentious, loaded with digressions, its argument buried under masses of fantastic, uncritical learning, the work of a vigorous but quite unoriginal mind. As Bolingbroke said, Cudworth "read too much to think enough, and admired too much to think freely." It is no calamity that natural procrastination, or the clamour caused by his candid treatment of atheism and by certain heretical tendencies detected by orthodox criticism in his view of the Trinity, made Cudworth leave the work unfinished.