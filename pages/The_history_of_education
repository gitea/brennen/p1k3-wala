<[[Brennen]]> A page hanging around from a much earlier iteration of this wiki. I think the rest of this is from [[Stephen]]. SeeAlso: HistoryOfEducation, PhilosophyOfEducation.

----

It has become popular to consider all of our predecessors grunting ignorant savages (in the distant past), or speaking ignorant savages (in the closest past that has no living example - the late 1800s). This is not so. Humanity, during its long existence, has always ''always'' been blindingly clever.

You know the principle: individuals are smart, but all groups are idiotic. Take this further => the masses. Made up of collections of groups, the mass's only common interest is food, sleep, and entertainment -- and they will go to great lengths to achieve those goals.

''(Note that some, such as MalcolmGladwell in ''TheTippingPoint'', suggest that groups are not idiotic; they are merely very different organisms than individual humans)''

This includes the invention of devices for greater food gathering, safer and more comfortable sleep, and more interesting and varied entertainment.  Once inventions have been created that prove themselves to further these causes, the masses will desire their continued production.  This requires a data archive to stores the methods and means to re-create these inventions.

Nowadays one would first think of a written record to store project notes and other material details.  But in the era that preceeded the abstraction of the written word, speech as an archival method had to suffice.  An inventor archiving knowledge into a single individual provided only doubled the chances that the knowledge would survive.  Archiving to groups of individuals increased the chances for the survival of the knowledge to the point that it became much more likely that the knowledge would survive than fade into forgotten history.

Eventually those that watched and studied the world, those that created tools, harnessed fire, outlined cooperative attack strategies capable of bringing down mammoths had discovered so much that youths growing up in the communities were required to constantly learn and observe so that they would be capable of surviving and adding to the depth of knowledge that was accumulating.

Everything since is just upgrades.

''Or kludgy bugfixes.''

<[[BrentNewhall]]> The above is definitely not mine.  I like it, but it's not mine.

<[[Brennen]]> Removed potential attribution.