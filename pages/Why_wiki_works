<[[Brennen]]> This page is very old and reflects my thinking before I had used wiki much. It is here as an archival object. That said, I still agree with most of the intent.

----

Wiki works because most of its attributes support each other.  Wiki works because it is simple. Wiki works because it's imperfect.

Wiki works when you realize two things: NothingIsSacred and EverythingIsSacred.  You can create anything here, and you can destroy anything here.  Go ahead.  (Except: Preserve discussions between users. We want those.) That's the point.  [[It's supposed to be messy]].  Just remember the corollary: It's all worth something.

This wiki (see WalaCode for software info) is sort of a special case, since I'm using it both as comment software for my weblog and as a general tracking system for ideas and objects. (See KnowItExists.)

Some recommended practices:

what to do
----------

* Delete or condense useless text. Do _not_, however, destructively rewrite/condense discussions between users. Wala is intended for [[IRC Mode]] conversation.
* Add content to any page.
* Add new pages. This wala is meant to run on the principal of KnowItExists - the idea that an entry for any given idea or object is useful, because just knowing something is out there is half the battle.
* Use existing pages like LinkDump and IdeaLogging for interesting fragments.
* Store relevant URLs here.
* Correct typos, grammar errors, etc.
* Remove spam when you find it.
* It helps if you log in before editing or commenting on a page.

what NOT to do
--------------

* Do not disagree by deleting, in which you delete text simply because you disagree with it.  Even if that text is strongly-worded, at most you should refactor it to be more fair. If the text is explicitly part of a discussion between other users, generally you should leave it alone, though moving it to a more appropriate location is probably fine. The Management promises to be fairly reasonable about these sorts of things while reserving the right to do whatever the hell it feels like.