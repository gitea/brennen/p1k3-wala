WareLogging, IdeaLogging

<[[Brennen]]> What's the simplest non-stupid way to handle this?

<[[Alan]]> a small transaction window where you lock and perform an out-of-date test

<[[Brennen]]> This much makes sense; next question is, how do I make the interface non-stupid? That is, I don't just want to prevent an overwrite and notify the user - I want to make merging changes back in fairly straightforward.

Present both versions side by side? A textarea with the older version alongside?

I also thought about some kind of automatic forking - "This page has multiple versions, please resolve the differences" - but that gets complicated in a hurry.


<[[alan]]> Ha, now you see what I was talking about in that [http://blogs.thegotonerd.com/maelstrom/archives/000650.html rethinking wikis in terms of version control] post. This is the merge problem. It's non-trivial to solve, which is why you have an entire version control system like [http://codeville.org/ codeville] which "began with a novel idea for a merge algorithm and has grown from there."

The (somewhat) standard approach to an out-of-date conflict on the contents of a file is to fail the commit, then produce a 3-way mergefile. This consists of your delta interwoven with the delta that has accumulated behind your back, since you started editing. You force the committer to comb through this file and selectively accept or hand-merge competing diff hunks. One place I worked (not naming any names here) had a remarkable command line / graphical merge tool for doing this.

So at a minimum I think you need something like diff3(1) that can generate the 3-way mergefile, and then a merge resolver that lets the user process the output of that.
